import 'babel-polyfill'
import Vue from 'vue'
// vuex data store. see the files in this path for actions and mutations
import store from './store'
// app root, see the other components for app layout
import App from './components/App.vue'

new Vue({
	store,
	'el': '#app',
	'components': { App }
});
