'use strict';
/**
 * Actions defining reactive or visual updates to the application
 *
 *
 * (This runs embedded in the hub where we automatically have jQuery since it's included in the other template.
 * I wouldn't use it if it wasn't already there but since it is it's convenient.)
 *
 * (Also there is some purely visual stuff I just do directly rather than creating reactive state for it,
 * and I would probably not do so in a rewrite)
 */

const
	bibTeXParser = require('bibtex-parse-js'), // implements a handy .bib -> JSON
	md5 = require('md5') // not used cryptographically, just to make opaque ids out of normalized author names
	;

// process bibtext into a list of citations contained therein. there is some support for endnote as well, but not endnote library (.enl) files
export const processFiles = ({ commit }, evt) => {
	if (!evt.dataTransfer || !evt.dataTransfer.files.length) {
		if (evt.target.files && evt.target.files.length) {
			evt.dataTransfer = evt.target;
		}
		else {
			return;
		}
	}

	for (let idx = 0; idx < evt.dataTransfer.files.length; ++idx) {
		const name = evt.dataTransfer.files[idx].name;
		let type = name.match(/[.](bib(tex)?|end(note)?)$/i);
		if (!type) {
			continue;
		}
		type = type[1].toLowerCase().replace(/tex|note/, '');
		commit('PREP_FILE_NAME', { name, type });
		read(evt.dataTransfer.files[idx], type)
			.then(cleanBibTeX(type))
			.then(bibTeXParser.toJSON)
			.then(cleanJson)
			.then((json) => {
				commit('ADD_CITATIONS', { name, json });
			});
	}
	$(window).bind('beforeunload', function() {
		if (!window.downloading) {
			return 'Before leaving, make sure you have downloaded the file if you have made any changes you\'d like to save.';
		}
	});
};

export const undo = ({ commit, state }) => {
	commit('UNDO');
	searchNames({ commit, state }, { 'target': $('.disam.search') });
};


/**
 * The PHP implements a service that compares the names in the citations to the ones in our user database to see if we have any (near) matches
 *
 * This is the interface for that
 */
export const getSimilarNames = ({ commit }, evt) => {
	const inp = $(evt.target);
	$.ajax({
		'url': `${location.pathname.replace(/\/$/, '')}/matchNames`,
		'method': 'GET',
		'data': {
			'names': inp.val()
		},
		'success': (res) => {
			const
				lbl = inp.parent().find('p.similar-label').hide(),
				ol  = inp.parent().find('ol.similar').empty().hide();

			if (res.result && res.result.length) {
				lbl.show();
				ol.show();
				res.result.forEach((name) => {
					ol.append($('<li>').text(name[1] + (name[2] ? ': ' + name[2] : '')));
				});
			}
		}
	});
}
// space-saver to hide the above when it doesn't appear contextually useful
export const hideSimilarNames = ({ commit }, evt) => {
	const prnt = $(evt.target).parent();
	prnt.find('p.similar-label').hide();
	prnt.find('ol.similar').hide();
}


/**
 * The visualizations called by this app group people by their names, and there's a real problem with variations being bucketed separately:
 *
 * Steven Snyder
 * steven snyder
 * Steveñ Sñyder
 * Steven J Snyder
 *
 * This normalization maps those all to the same id, which helps. There are still a few things that need to be attended to:
 *
 * Steven James Snyder -- initials are discard but adding other full portions of the name gets a different id
 * Steven S -- likewise initializing a portion of something listed as a full name elsewhere
 * Steve Snyder -- variants that aren't handled by transliteration
 *
 * NB: transl() is defined in another script linked on the browser
 */

export const normalizeName = (name) => {
	// approximate non-ascii chars
	return transl(name)
		// normalize case
		.toLowerCase()
		// trim leading and trailing space, remove stuff between braces (BibTeX escaping), remove non-alpha non-space characters
		.replace(/^\s+|\s+$|\{.*?\}|[^a-z\s]/g, '')
		// split by any number of spaces
		.split(/\s+/)
		// discard initials
		.filter((part) => { return part.length > 1; })
		// put the parts in alphabetical order so first-last vs last-first doesn't influence the id
		.sort()
		.join(' ');
}
function nameToId(name) {
	// hash only to make the id opaque (ensures changing the formula in the future will not violate formatting expectations)
	return md5(normalizeName(name));
}

// debounce timer
let searchDB = null;

/**
 * To aid in the remaining name form reconciliation hassles, you can do a substring search on the names and combine any of the results that look like they are the same person
 */
export const searchNames = ({ commit, state }, evt) => {
	if (searchDB) {
		clearInterval(searchDB);
	}
	searchDB = setTimeout(() => {
		const authorMap = {};
		const targ = $(evt.target), makeRe = (val) => {
			return new RegExp(val, 'i');
		}, find = (val, re, bibIdx) => {
			// runs once for each citation -- if we're on the first one clear any existing results
			if (bibIdx == 0) {
				commit('CLEAR_AUTHOR_SEARCH');
			}
			let newVal = strip(targ.val());
			if (newVal == '') {
				newVal = ' ';
			}
			// we're not searching for the same thing we were last iteration. clear and start over so they don't see inaccurate older results
			if (newVal != val) {
				if (newVal == '') {
					commit('CLEAR_AUTHOR_SEARCH');
					return;
				}
				return find(newVal, makeRe(newVal), 0);
			}

			// test authors in the current citation
			state.bibtex[bibIdx].entryTags.author.forEach((author) => {
				if (re.test(author)) {
					if (!authorMap[author]) {
						authorMap[author] = 0;
					}
					++authorMap[author];
				}
			});

			// check the next
			if (bibIdx < state.bibtex.length - 1) {
				find(val, re, bibIdx + 1);
			}
			// or, lacking a next, finalize the results
			else {
				commit('SHOW_AUTHOR_SEARCH_RESULTS', authorMap);
			}
		};

		// kick off search based on form data
		let val = strip(targ.val());
		if (val == '') {
			val = ' ';
		}
		const re = makeRe(val);
		checkNoAuthors();
		find(val, re, 0);
	}, 300); // this is a little slow for large files, so we debounce to prevent the input from lagging while typing quickly
}

// helpers for name reconciliator
export const checkAllAuthors = (_, evt) => {
	$('.author-results input').prop('checked', true);
}
export const checkNoAuthors = (_, evt) => {
	$('.author-results input').prop('checked', false);
}
export const setPreferredName = ({ commit }, evt) => {
	$(evt.target).prev().prop('checked', true);
	$('.disam.choice').val($(evt.target).text());
}

// "go" button for disambiguation to change all the checked variations to the selected canonical form
export const foldAuthors = ({ commit, state }) => {
	let geo = $('.disam.geo');
	if (strip(geo.val())) {
		// don't propagate a bad location
		if (geo.hasClass('bad')) {
			return;
		}
		// poll until resolution is complete
		if (!geo.hasClass('good') || !geo.data('geolookup')) {
			setTimeout(() => { foldAuthors({ commit, state }); }, 100);
			return;
		}
	}
	const to = $('.disam.choice').val(),
		from = [];
	$('.author-results input:checked').each((idx, el) => {
		from.push($(el).next().text());
	});
	if (!from.length) {
		return;
	}
	$('.author-results').addClass('loading');
	geo = geo.data('geolookup');
	if (geo) {
		geo = JSON.parse(geo);
	}
	else {
		geo = null;
	}
	commit('FOLD_AUTHORS', { to, from, geo });
	searchNames({ commit, state }, { 'target': $('input.disam') });
	$('.author-results').removeClass('loading');

};


// the list of citations gets way too long with the editor always visible, make it click-to-view
export const showEditor = ({ commit }, cite) => {
	if (!cite.removed) {
		const ed = $('#edit-' + cite.id).show('slow');
		ed.parent().addClass('editor-open');
	}
}
export const hideEditor = ({ commit }, id) => {
	const ed = $('#edit-' + id).hide('slow');
	ed.parent().removeClass('editor-open');
}

// sets the removed flag on a citation, which makes it appear at partial opacity and be disincluded from any of the export methods
export const removeEntry = ({ commit }, id) => {
	hideEditor({ commit }, id);
	commit('REMOVE_CITATION', id);
}
// undo for removeEntry
export const restoreEntry = ({ commit }, id) => {
	commit('RESTORE_CITATION', id);
}

// same as removeEntry, but operates on a single (k, v) pair of attributes in an entry rather than the whole citation
export const removeTag = ({ commit }, tag) => {
	console.log({ tag });
	commit('REMOVE_TAG', tag);
}
export const restoreTag = ({ commit }, tag) => {
	commit('RESTORE_TAG', tag);
}

// save edits to the <input>s representing tags
export const editTag = ({ commit }, evt) => {
	const inp = $(evt.target), li = containingLI(inp);
	commit('EDIT_TAG', { 'tag': li.data('tag'), 'value': inp.val() });
}

// add entirely new tags to the interface
// @TODO consider removing or amending -- tags that aren't in the bibtex spec don't survive the export process
export const addTag = ({ commit }, evt) => {
	const
		li = containingLI(evt.target),
		key = li.find('.key'),
		val = li.find('.value')
		;

	if (strip(key.val()).length && strip(val.val()).length) {
		commit('ADD_TAG', { 'citation': li.data('citation-id'), 'key': key.val(), 'value': val.val() });
	}
	key.val('');
	val.val('');
}

export const changeSex = ({ commit }, evt) => {
	const el = $(evt.target), prnt = el.parent();
	prnt.children('button').removeClass('active');
	el.addClass('active');
	commit('CHANGE_SEX', { 'author': prnt.data('author-name'), 'sex': el.data('value') })
}

export const changeRole = ({ commit }, evt) => {
	const el = $(evt.target);
	commit('CHANGE_ROLE', { 'author': el.data('author-name'), 'role': el.val() });
}

/**
 * roles: student, staff, postdoc, faculty
 * Some visualizations utilize geolocation data to plot citations on a map.
 *
 * Call back here to try to convert whatever they type into the location box into lat, lon.
 *
 * The server defers to Google's geocoding service to figure this out. It's pretty nice in that
 * it doesn't require an API key and it's flexible -- zip codes, city names, state names, even many
 * landmark names ("eiffel tower") work.
 */
export const geolookup = ({ commit }, evt) => {
	const inp = $(evt.target);
	if (!strip(inp.val())) {
		inp.removeClass('bad').removeClass('good');
		return;
	}
	const isDisam = inp.hasClass('disam');
	$.ajax({
		'url': '/erc-citations/geocode',
		'method': 'GET',
		'data': { 'place': inp.val() },
		'success': (resp) => {
			// store resolution and visually mark good
			if (resp.results && resp.results[0]) {
				const geo = resp.results[0].geometry;
				const data = [
					resp.results[0].formatted_address,
					[
						[ geo.location.lat, geo.location.lng ],
						resp.results[0].address_components[0].long_name,
						[ null, null ] // deprecated bit
					]
				];
				if (isDisam) {
					inp.data('geolookup', JSON.stringify(data));
				}
				else {
					commit('SET_GEOCODING', { 'idx': inp.data('idx'), 'data': data });
				}
				inp.removeClass('bad').addClass('good').val(resp.results[0].formatted_address);
			}
			// visually mark as needing review
			else {
				if (!isDisam) {
					commit('SET_GEOCODING', { 'idx': inp.data('idx'), 'data': [ inp.val(), undefined ] });
				}
				inp.removeClass('good').addClass('bad');
			}
		}
	})
};
// visually reset a geolocation to its default state
export const setGeoUndecided = (_, evt) => {
	$(evt.target).removeClass('good').removeClass('bad').data('geolookup', '')
}

/**
 * This method comes up with a JSON representation of the citations as they are on the page including any edits or removals,
 * and then makes a request to store that on our server. The store method returns a hash of this data that we pass along to
 * the visualization. The visualization can then call back using that hash to get access to the full data.
 *
 * We do that instead of posting the entire data to the visualization in the first place so that people have a URL with
 * ?hash=somehex that they can share or bookmark.
 *
 Supplied example data format from vis dev people:
{
    "records": {
        "schema": [{
            "name": "author_ids",
            "type": "string"
        }, {
            "name": "title",
            "type": "string"
        }, {
            "name": "journal",
            "type": "string"
        }, {
            "name": "year",
            "type": "numeric"
        }, {
            "name": "url",
            "type": "string"
        }, {
            "name": "citation_count", //ERC user on the MoS demo claimed to have citation info in their Bibtex. Inculde if it exists.
            "type": "numeric"
        }],
        "data": [{
            "author_ids": [1,3],
            "title": "Some Publication Title",
            "journal": "Some Journal Name",
            "year": 2007,
            "url": "google.com",
            "citation_count": 12
        }]
    },
    "authors": {
        "schema": [{
            "name": "id",
            "type": "string"
        }, {
            "name": "author",
            "type": "string"
        }, {
            "name": "affiliation",
            "type": "numeric"
        }, {
            "name": "lat",
            "type": "numeric"
        }, {
            "name": "lng",
            "type": "numeric"
        }, {
            "name": "role", //Facuty, student, etc
            "type": "string"
        }, {
            "name": "gender",
            "type": "string"
        }],
        "data": [{
            "id": 1,
            "author": "Simpson, Adam",
            "affiliation": "Indiana University",
            "lat": 0.00,
            "lng": 0.00,
            "role": "faculty",
            "gender": "Male"
        }, {
            "id": 3,
            "author": "Sampson, Sadam",
            "affiliation": "Indiana University - East",
            "lat": 0.00,
            "lng": 0.00,
            "role": "student",
            "gender": "Male"
        }]
    }
}
 */

const jsonData = (state) => {
	// document skellington
	const rv = {
		'records': {
			'schema': [{
				"name": "author_ids",
				"type": "string"
			}, {
				"name": "title",
				"type": "string"
			}, {
				"name": "journal",
				"type": "string"
			}, {
				"name": "year",
				"type": "numeric"
			}, {
				"name": "url",
				"type": "string"
			}, {
				"name": "citation_count", //ERC user on the MoS demo claimed to have citation info in their Bibtex. Inculde if it exists.
				"type": "numeric"
			}],
			'data': []
		},
		'authors': {
			"schema": [{
				"name": "id",
				"type": "string"
			}, {
				"name": "author",
				"type": "string"
			}, {
				"name": "affiliation",
				"type": "numeric"
			}, {
				"name": "lat",
				"type": "numeric"
			}, {
				"name": "lng",
				"type": "numeric"
			}, {
				"name": "role", //Facuty, student, etc
				"type": "string"
			}, {
				"name": "gender",
				"type": "string"
			}],
        "data": []
		}
	};
	const authors = {};
	state.bibtex.forEach((entry) => {
		const json = {
			'author_ids': [],
			'title': entry.entryTags.title,
			'journal': entry.entryTags.journal,
			'year': entry.entryTags.year ? parseInt(entry.entryTags.year) : null,
			'url': entry.entryTags.url,
			'citation_count': null
		};
		entry.tagList.forEach((tag) => {
			// structure authors according to example. need to make them ids to match up with the two main sections of the document
			if (tag.key == 'author') {
				tag.value.forEach((name) => {
					const id = nameToId(name);
					json.author_ids.push(id);
					if (!authors[id]) {
						authors[id] = {
							'id': id,
							'author': name,
							'affiliation': null,
							'lat': null,
							'lng': null,
							'zip': '',
							'role': state.authorMeta[name] ? state.authorMeta[name].role : null,
							'gender': state.authorMeta[name] ? (state.authorMeta[name].sex == 'm' ? 'male' : state.authorMeta[name].sex == 'f' ? 'female' : '') : null
						}
					}
				});
			}
			// we store some extraneous data about geocoding to make the UI look nicer, showing "Sometown, CA, USA" instead of [lat, lng]
			// according to the data format we just do the latter, though, so select those parts.
			if (tag.key == 'geocoding') {
				json.author_ids.forEach((authorId, idx) => {
					if (authors[authorId].lat === null && tag.value[idx][1]) {
						authors[authorId].lat = tag.value[idx][1][0][0];
						authors[authorId].lng = tag.value[idx][1][0][1];
						if (tag.value[idx][0]) {
							let zip;
							if (tag.value[idx][1] && typeof tag.value[idx][1] === 'string') {
								zip = tag.value[idx][1];
							}
							else {
								zip = tag.value[idx][0].match(/\d{5}/);
							}
							if (zip) {
								authors[authorId].zip = zip[0];
							}
						}
					}
				});
			}
		});
		rv.records.data.push(json);
	});
	// convert map of id => attrs for authors to an array of attrs
	for (const k in authors) {
		rv.authors.data.push(authors[k]);
	}
	return rv;
}
export const visualizeCoAuthorship = ({ state }) => {
	// store and ask for a hash that can be used to retreive again
	downloadTicket('json', JSON.stringify(jsonData(state)), (res) => {
		// attempt a pop-up to avoid losing any unsaved changes to the citation document
//		open(location.origin + '/labs/run/erc/coauthor.html?hash=' + res.ticket);
		open('https://lab3.nanohub.aws.hubzero.org/coauthor.html?hash=' + res.ticket);
	});
}

export const visualizeJournals = ({ state }) => {
	// store and ask for a hash that can be used to retreive again
	downloadTicket('json', JSON.stringify(jsonData(state)), (res) => {
		// attempt a pop-up to avoid losing any unsaved changes to the citation document
//		open(location.origin + '/labs/run/erc/scimap.html?hash=' + res.ticket);
		open('https://lab3.nanohub.aws.hubzero.org/scimap.html?hash=' + res.ticket);
	});
}

export const visualizeAuthorGeolocation = ({ state }) => {
	// store and ask for a hash that can be used to retreive again
	downloadTicket('json', JSON.stringify(jsonData(state)), (res) => {
		// attempt a pop-up to avoid losing any unsaved changes to the citation document
//		open(location.origin + '/labs/run/erc/prosym.html?hash=' + res.ticket);
		open('https://lab3.nanohub.aws.hubzero.org/prosym.html?hash=' + res.ticket);
	});
};;


export const downloadBibTeX = ({ state }) => {
	download('bib', state);
};

export const downloadEndNote = ({ state }) => {
	download('end', state);
};

/**
 * Store some data in the database and retrieve a hash for it that can be used to retrieve again.
 *
 * If a callback is not supplied it attempts to download/open it in the browser.
 *
 * Otherwise, the callback can decide what to do with the response.
 */
function downloadTicket(type, body, cb, user) {
	const files = [];
	$('.file-names li strong').each((idx, el) => {
		files.push($(el).text());
	})
	let fn = null;
	if (user) {
		const dt = new Date;
		fn = prompt('File name?',
			files
				.map((name) => { return name.replace(/[.].*?$/, ''); })
				.join('-') + '-' + dt.getFullYear() + '-' +
					((dt.getMonth() < 9 ? '0' : '') + (dt.getMonth() + 1)) + '-' +
					((dt.getDate() <= 9 ? '0' : '') + dt.getDate()) + '.bib');
		if (!fn) {
			return;
		}
	}
	$.ajax({
		'url': `${location.pathname.replace(/\/$/, '')}/downloadTicket`,
		'method': 'POST',
		'contentType': 'application/json',
		'data': JSON.stringify({ 'type': type, 'body': body, 'files': files, 'name': fn }),
		'success': cb ? cb : (resp) => {
			open(location.origin + resp.result);
		}
	});

}

/**
 * Convert the JSON structure of the citation state back to BibTeX and attempt to fetch it by bouncing it off the server
 */
function download(type, state) {
	const bibtex = [];
	state.bibtex.forEach((entry) => {
		if (entry.removed) {
			return;
		}
		bibtex.push(`@article{${entry.citationKey},\n`);

		const authorMeta = entry.entryTags.author.map((name) => {
			return state.authorMeta[name].sex + '.' + state.authorMeta[name].role;
		}).join(':');

		entry.tagList.forEach((tag) => {
			if (tag.removed) {
				return;
			}
//			if (tag.key === 'geocoding') {
//				bibtex.push('\tnote = ' + (entry.entryTags.note ? entry.entryTags.note + ' ' : '') + JSON.stringify(JSON.stringify({ 'geocoding': tag.value })) + '\n');
//			}
			if (tag.key !== 'note' && tag.key !== 'geocoding' && tag.key !== 'authorMeta') {
				bibtex.push(`\t${tag.key} = ${JSON.stringify(tag.key === 'author' ? tag.value.join(' and ') : tag.value)},\n`);
			}
		});
		bibtex.push(
			'\tnote = ' + JSON.stringify(JSON.stringify({ 'geocoding' : entry.entryTags.geocoding })) + 'authorMeta:' + authorMeta + '\n'
		);
		bibtex.push('}\n\n');
	});
	downloadTicket(type, bibtex.join(''), (resp) => {
		// try to prevent the beforeunload handler from complaining
		window.downloading = true;
		window.location = location.origin + resp.result;
		setTimeout(() => {
			window.downloading = false;
		}, 5000);
	}, true);
}

// remove leading and trailing whitespace
function strip(str) {
	return str.replace(/^\s+|\s+$/g, '');
}

// find the proximate <li> that contains a given element
function containingLI(el) {
	let rv = $(el).parent();
	while (rv.prop('tagName') !== 'LI') {
		rv = rv.parent();
	}
	return rv;
}

// drag-n-dropped file -> file data
function read(file, type) {
	return new Promise((resolve, reject) => {
		let reader = new FileReader;
		reader.onerror = (err) => {
			reject(err);
		};
		reader.onload = (evt) => {
			resolve(evt.target.result);
		};
		reader.readAsText(file);
	});
}

// posts to the server, which invokes bibutils' (bib|end)2xml and xml2bib to
// (a) normalize both supported formats to BibTeX
// (b) convert BibTeX's pre-utf8 weird escapes to normal utf8
// (c) prevent shenanigans, since data that doesn't match BibTeX spec won't survive
function cleanBibTeX(type) {
	return (body) => {
		return new Promise((resolve, reject) => {
			$.ajax({
				'url': `${location.pathname.replace(/\/$/, '')}/cleanBibTeX`,
				'method': 'POST',
				'contentType': 'application/json',
				'data': JSON.stringify({ type, body }),
				'success': (resp) => {
					if (resp.result) {
						resolve(resp.result);
					}
					else {
						reject();
					}
				},
				'error': reject
			});
		});
	};
}

/**
 * The direct output of BibTeX->JSON isn't exactly what we want in the state.
 *
 * We add a bit more structure here.
 */
function cleanJson(json) {
	json.forEach((row) => {
		Object.keys(row.entryTags).forEach((k) => {
			row.entryTags[k] = row.entryTags[k].replace(/\\&/g, '&');
		});
		// authors are an 'and'-delimited string which we'd prefer as an array
		if (row.entryTags.author) {
			row.entryTags.author = row.entryTags.author.split(/[\r\n\s]+and[\r\n\s]+/gi);
		}
		else {
			row.entryTags.author = [];
		}
		// since there's no BibTeX field for geocoding we sneak it in the note field
		// due to the encoding that happens to it there it's a bit messy (but workable)
		// to extract the data back out
		if (row.entryTags.note) {
			const geo2 = /geocoding.*?(\[\[.*\]\])/.exec(row.entryTags.note);
			if (geo2) {
				try {
					row.entryTags.geocoding = JSON.parse(geo2[1].replace(/\\[?]/g, '"').replace(/[?]/g, '"A'));
				}
				catch (ex) {
					console.log(ex, geo2);
				}
			}
			const authorMeta = /authorMeta:(.*)/.exec(row.entryTags.note);
			if (authorMeta) {
				row.entryTags.authorMeta = authorMeta[1].split(':').map((x) => {
					x = x.split('.');
					return {
						'sex': x[0] == 'm' ? 'm' : x[0] == 'f' ? 'f' : undefined,
						'role': x[1] == 'undefined' ? undefined : x[1]
					};
				});
			}
		}
		// logic is simpler elsewhere if we always assume this array exists and is
		// 2 elements long, so if we don't have prior data just sketch out a template for it
		if (!row.entryTags.author) {
		}
		if (!row.entryTags.geocoding) {
			row.entryTags.geocoding = row.entryTags.author.map(() => { return [undefined, undefined]; });
		}
		if (!row.entryTags.authorMeta) {
			row.entryTags.authorMeta = row.entryTags.author.map(() => { return { 'sex': undefined, 'role': undefined }; });
		}
	});
	return json;
}

