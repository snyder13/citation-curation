const dataAttrRe = /^data-(.*)$/;
const readSeedData = (store) => {
	Array.from(document.querySelectorAll('.vuex-seed')).forEach((el) => {
		Object.keys(el.attributes).forEach((key) => {
			const ma = el.attributes[key].name.match(dataAttrRe);
			if (ma) {
				store.commit('SEED', {
					'key'  : ma[1],
					'value': JSON.parse(el.attributes[key].value)
				});
			}
		});
	});
};

export default [ readSeedData ];
