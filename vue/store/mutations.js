export const state = {
	'files': [],
	'file-indices': {},
	'bibtex': [],
	'cited': {},
	'authorResults': [ ],
	'authorMap': {},
	'visualizations': [],
	'authorMeta': {},
	'undoState': null,
	'lastAuthorFold': {}
}

import Vue from 'vue';

import { normalizeName } from './actions.js';

const prod = (id) => {
	Vue.set(state.bibtex, id, state.bibtex[id]);
};


export const mutations = {
	UNDO(state) {
		if (state.undoState === null) {
			return;
		}
		state.authorMeta = state.undoState.authorMeta;
		state.bibtex = state.undoState.bibtex;
		state.undoState = null;
		state.lastAuthorFold = {};
	},
	/**
	 * Give a list of author names `from` and a normalized form `to` convert all the former to the latter
	 *
		state.bibtex[idx.citation].entryTags.geocoding[idx.author] = data;
		state.bibtex[idx.citation].tagList.some((tag) => {
			if (tag.key == 'geocoding') {
				tag.value[idx.author] = data;
				return true;
			}
		});

	 */
	FOLD_AUTHORS(state, { from, to, geo }) {
		const undoState = {
			'authorMeta': JSON.parse(JSON.stringify(state.authorMeta)),
			'bibtex': JSON.parse(JSON.stringify(state.bibtex))
		};
		const tmp = JSON.parse(JSON.stringify(undoState));
		tmp.lastAuthorFold = {};
		let fromMap = {};
		from.forEach((name) => {
			fromMap[name] = true;
			if (tmp.authorMeta[from]) {
				tmp.lastAuthorFold[from] = {
					'meta': tmp.authorMeta[from],
					'count': 0
				};
				tmp.authorMeta[to] = tmp.authorMeta[from];
			}
		});

		tmp.bibtex.forEach((cite, cidx) => {
			cite.entryTags.author.forEach((author, idx) =>  {
				if (fromMap[author]) {
					if (!tmp.lastAuthorFold[author]) {
						tmp.lastAuthorFold[author] = { 'count': 0 };
					}
					++tmp.lastAuthorFold[author].count;
					cite.entryTags.author.splice(idx, 1, to);
					if (geo) {
						cite.entryTags.geocoding[idx] = geo;
						cite.tagList.some((tag) => {
							if (tag.key === 'geocoding') {
								tag.value[idx] = geo;
								return true;
							}
						});
					}
				}
			});
			cite.tagList.some((tl, tidx) =>  {
				if (tl.key == 'author') {
					cite.entryTags.author.forEach((author, idx) => {
						cite.tagList[tidx].value.splice(idx, 1, cite.entryTags.author[idx]);
					});
					return true;
				}
			});

		});
		state.lastAuthorFold = {
			'name': to,
			'matches': Object.keys(tmp.lastAuthorFold)
				.map((k) => {
					return { 'name': k, 'meta': tmp.lastAuthorFold[k].meta, 'count': tmp.lastAuthorFold[k].count };
				})
				.filter((a) => {
					return a.name !== to;
				})
				.sort((a, b) => {
					return a.name.localeCompare(b.name);
				})
		};
		state.authorMeta = tmp.authorMeta;
		state.bibtex = tmp.bibtex;
		state.undoState = undoState;
	},
	CLEAR_AUTHOR_SEARCH(state) {
		state['authorResults'] = [];
		state['authorMap'] = {};
	},
	/**
	 * Finalize results by sorting and adding them to the state
	 */
	SHOW_AUTHOR_SEARCH_RESULTS(state, authorMap) {
		let tmp = [];
		Object.keys(authorMap).forEach((name) => {
			const res = { 'name': name, 'count': authorMap[name], 'normal': normalizeName(name) };
			// splice the results into the array at the appropriate place
			// using sort() fails to trigger a redraw and the items remain
			// in their original order
			let idx = 0;
			for (; idx < tmp.length && tmp[idx].name < res.name; ++idx) {
				// this space intentionally left blank
			}
			tmp.splice(idx, 0, res);
		});
		state.authorResults = tmp;
	},
	/**
	 * Show that we're loading a dropped file by adding it to the files list with a throbber
	 */
	PREP_FILE_NAME(state, { name, type }) {
		state['file-indices'][name] = state.files.length;
		state.files.push({
			name,
			'state': 'queued',
			'type': type
		});
	},
	/**
	 * Add a bundle of citations from json
	 */
	ADD_CITATIONS(state, { name, json }) {
		const file = state.files[state['file-indices'][name]];
		file.dupes = 0;
		const tmp = {
			'bibtex': JSON.parse(JSON.stringify(state.bibtex)),
			'authorMeta': JSON.parse(JSON.stringify(state.authorMeta)),
			'cited': JSON.parse(JSON.stringify(state.cited))
		};
		json.forEach((entry) => {
			if (state.cited[entry.citationKey]) {
				++file.dupes;
				console.log({ 'dupe': entry });
			}
			else {
				entry.id = tmp.bibtex.length;
				entry.tagList = [];
				let tagId = 0;
				Object.keys(entry.entryTags).forEach((tag) => {
					// often convenient to have tags a list in addition to the map
					entry.tagList.push({
						'key': tag,
						'value': entry.entryTags[tag],
						'id': tagId++,
						'citation': entry.id
					});
				});
				tmp.bibtex.push(entry);
				tmp.cited[entry.citationKey] = true;
			}
			if (entry.entryTags.authorMeta) {
				entry.entryTags.authorMeta.forEach((meta, idx) => {
					tmp.authorMeta[entry.entryTags.author[idx]] = meta;
				});
			}
		});

		state.cited = tmp.cited;
		state.authorMeta = tmp.authorMeta;
		state.bibtex = tmp.bibtex;
		file.count = json.length;
		file.state = 'done';
	},
	/**
	 * Opt citation out of export methods
	 */
	REMOVE_CITATION(state, id) {
		state.bibtex[id].removed = true;
		prod(id);
	},
	/**
	 * Opt back in
	 */
	RESTORE_CITATION(state, id) {
		state.bibtex[id].removed = false;
		prod(id);
	},
	/**
	 * Opt tag out of export methods
	 */
	REMOVE_TAG(state, tag) {
		state.bibtex[tag.citation].tagList[tag.id].removed = true;
		/// @TODO rm awful hack. can't get this to update on the boolean change
		state.bibtex[tag.citation].tagList[tag.id].value += ' ';
		state.bibtex[tag.citation].tagList[tag.id].value = state.bibtex[tag.citation].tagList[tag.id].value.replace(/ $/, '');
	},
	/**
	 * Opt back in
	 */
	RESTORE_TAG(state, tag) {
		state.bibtex[tag.citation].tagList[tag.id].removed = false;
		state.bibtex[tag.citation].tagList[tag.id].value += ' ';
		state.bibtex[tag.citation].tagList[tag.id].value = state.bibtex[tag.citation].tagList[tag.id].value.replace(/ $/, '');
	},
	/**
	 * Change a tag given the contents of <input>s representing it
	 */
	EDIT_TAG(state, { tag, value }) {
		state.bibtex[tag.citation].tagList[tag.id].value = value;
	},
	/**
	 * Add an entirely new tag
	 */
	ADD_TAG(state, { citation, key, value }) {
		state.bibtex[citation].tagList.push({
			'key': key,
			'value': value,
			'citation': citation,
			'id': state.bibtex[citation].tagList.length
		});
	},
	/**
	 * Set the geocoded location for a given author
	 */
	SET_GEOCODING(state, { idx, data }) {
		state.bibtex[idx.citation].entryTags.geocoding[idx.author] = data;
		state.bibtex[idx.citation].tagList.some((tag) => {
			if (tag.key == 'geocoding') {
				tag.value[idx.author] = data;
				return true;
			}
		});
	},
	SEED(state, { key, value }) {
		state[key] = value;
	},
	CHANGE_SEX(state, { author, sex }) {
		state.authorMeta[author].sex = sex;
	},
	CHANGE_ROLE(state, { author, role }) {
		console.log('role', author, role);
		state.authorMeta[author].role = role;
	}
}
