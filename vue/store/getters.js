// count the # of citations that correspond to journals for the (defunct) journal visualization
export const journalCount = (state) => {
	let count = 0;
	state.bibtex.forEach((entry) => {
		if (entry.entryTags.journal) {
			++count;
		}
	});
	return count;
};
