#!/usr/bin/env node

/**
 * This script will split a BibTeX file into multiple files, one for each year, and one for undefined years where required.
 *
 * It requires: Linux (maybe OSX), new-ish node.js, npm, xml2js, q
 *
 * In the same path as this script, run `npm install xml2js q` to get those.
 *
 * Bibutils should be in your distro package manager
 *
 * For a file foo.bib this will create foo-1999.bib, foo-2000.bib, ..., and maybe foo-unknown.bib in the same path
 */
const
	q          = require('q'),
	exec       = q.denodeify(require('child_process').exec),
	basename   = (path) => { return path.replace(/^.*\//, ''); },
	xml2js     = require('xml2js'),
	parseXml   = q.denodeify(xml2js.parseString),
	fs         = require('fs'),
	xmlBuilder = new xml2js.Builder
	;

if (process.argv.length < 3) {
	console.log(`split bibtex citations by year, usage: ${ basename(process.argv[0]) } ${ basename(process.argv[1]) } /path/to/a/bibtexfile.bib`);
	process.exit();
}

exec(`bib2xml ${process.argv[2]}`, { 'maxBuffer': 1024*500 })
	.then((data) => {
		console.log(process.argv[2], '\n', data[1]); // stderr, bib2xml puts out some useful summary info here for some reason
		return parseXml(data[0]); // stdout
	}, console.error)
	.then((xml) => {
		// split up by year
		const byYear = {};
		xml.modsCollection.mods.forEach((mod) => {
			const year = mod.originInfo && mod.originInfo[0].dateIssued ? mod.originInfo[0].dateIssued[0].replace(/\D|^\s+|\s+$/g, '') : '';
			if (!byYear[year]) {
				byYear[year] = [];
			}
			byYear[year].push(mod);
		});
		// write out
		Object.keys(byYear).forEach((year) => {
			const
				name  = process.argv[2].replace(/[.]bib$/, `-${ year == '' ? 'unknown' : year }.bib`),
				iname = name.replace(/[.]bib$/, '.xml')
				xml   = xmlBuilder.buildObject({ 'modsCollection': {
					'$': { 'xmlns': 'http://www.loc.gov/mods/v3' },
					'mods': byYear[year]
				} })
				;
			fs.writeFileSync(iname, xml, { 'encoding': 'utf8' });
			exec(`xml2bib ${iname}`)
				.then((data) => {
					console.log(name);
					console.log(data[1]); // gets # of citations for each year
					fs.writeFileSync(name, data[0]);
					fs.unlinkSync(iname);
				});
		});
	}, console.error);
