This application uses webpack to build a bundle for deployment. To make changes:

 1. Clone this repository (https://bitbucket.org/snyder13/citation-curation)
 2. Run `npm install` to get dependencies
 3. Edit the source files in the `vue/` path as appropriate
 4. Run `node_modules/.bin/webpack` to build immediately, or run `node_modules/.bin/webpack -w` to constantly scan for changes in `vue/` and make new builds as needed.
 5. Copy the `dist/` folder to the appropriate place on the hub. (ie., `app/components/com_citations/curate`)
