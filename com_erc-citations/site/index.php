<?php $mediaRoot = str_replace(JPATH_BASE, '', __DIR__); ?>
<div id="browsers" style="display: none">
	<p>Supported browsers:</p>
	<ul>
		<li><a href="https://www.google.com/chrome/browser/desktop/index.html">Google Chrome</a></li>
		<li><a href="https://www.microsoft.com/en-us/windows/microsoft-edge">Microsoft Edge</a></li>
		<li><a href="https://www.mozilla.org/en-US/firefox">Mozilla Firefox</a></li>
		<li><a href="https://support.apple.com/downloads/safari">Apple Safari</a></li>
	</ul>
</div>
<div id="app" class="vuex-seed" data-visualizations="<?php echo str_replace('"', '&quot;', json_encode($visualizations)); ?>">
	<app />
</div>
<script src="<?php echo $mediaRoot; ?>/dist/transliteration.min.js"></script>
<script src="<?php echo $mediaRoot; ?>/dist/build.js"></script>
<script>
jQuery(function($) {
	if (document.documentMode) {
		$('#browsers').show();
	}
})
</script>
